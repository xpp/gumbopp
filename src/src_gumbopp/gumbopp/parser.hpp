#pragma once

#include <any>
#include <memory>

#include "node.hpp"

namespace gumbopp::detail
{
    /**
     * @brief Base class for the parser. Provides basic functions and access
     * to all nodes.
     *
     * This class helps to keep all headers from gumbo-parser out of all headers
     * required by a user.
     * To enable allocators we have to erase their types.
     * This is done using virtual functions.
     */
    class parser
    {
        friend class node::element_node;
        friend class node::attribute_node;
        friend class node::text_node;
        friend class node::document_node;
        friend class node::error_node;
    public:
        const GumboOptions& gumbo_options() const;
        const GumboOutput& gumbo_output() const;
        const node::element_node& root() const;
        const node::document_node& document() const;

        const upp::span<node::element_node  >& all_elements() const;
        const upp::span<node::attribute_node>& all_attributes() const;
        const upp::span<node::text_node     >& all_texts() const;
    protected:
        ///@brief calls /ref init(buffer, kGumboDefaultOptions)
        void init(const char* buffer);
        /**
         * @brief Parses the text and builds the abstraction tree
         * @param buffer The html page
         * @param opt Options passed to gumbo-parser
         *
         * This function is called in the ctor of the derived class.
         * Since the derived class implements the virtual functions the calls
         * to virtual functions are dispatched correctly.
         *
         * /note https://en.cppreference.com/w/cpp/language/virtual#During_construction_and_destruction
         *
         * Calls /ref init_all_elem, /ref init_all_attr, /ref init_all_text
         * directly and calls
         * /ref emplace_elem, /ref emplace_attr, /ref emplace_text
         * indirectly.
         *
         *
         */
        void init(const char* buffer, const GumboOptions& opt);
        //protected -> only derived classes can construct (abstract anyways)
        parser() = default;
        parser(parser&&) = default;
        parser(const parser&) = default;
        virtual ~parser();

        //type erase the allocator
        virtual void init_all_elem(std::size_t n) = 0;
        virtual void init_all_attr(std::size_t n) = 0;
        virtual void init_all_text(std::size_t n) = 0;
        virtual void emplace_elem(
            node::element_node* p,
            const GumboNode& n,
            std::size_t& oelem,
            std::size_t& oattr,
            std::size_t& otext
        ) = 0;
        virtual void emplace_attr(node::attribute_node* p, const GumboAttribute& a) = 0;
        virtual void emplace_text(node::text_node* p, const GumboNode& n) = 0;
    protected:
        //gumbo
        std::any _options;
        GumboOutput* _output{nullptr};
        //data
        node::document_node _document;
        upp::span<node::element_node> _all_elem;
        upp::span<node::text_node> _all_text;
        upp::span<node::attribute_node> _all_attr;
        //        std::vector<node::error_node> _errors;

        const node::element_node* _root{nullptr};

        ///TODO ERRORS
    };
}

namespace gumbopp
{
    /**
     * @brief Parses a html page, owns the parse tree and offers querry capabilities.
     *
     * Holds all nodes of the c++ abstraction (or rather the base holds all data).
     * This enables us to
     * /li reduce the number of heap allocations to three (independend of the number of nodes)
     * /li copy singular nodes (since they are not owning)
     * /li quickly run a querry over all nodes at the same time.
     * /li use allocators with limited hassle.
     */
    template
    <
        class AllocatorElem = std::allocator<node::element_node>,
        class AllocatorAttr = std::allocator<node::attribute_node>,
        class AllocatorText = std::allocator<node::text_node>
        >
    class parser : public detail::parser
    {
    public:
        parser(
            const char* buffer,
            const AllocatorElem& ae = {},
            const AllocatorAttr& aa = {},
            const AllocatorText& at = {}
        );
        parser(
            const char* buffer,
            const GumboOptions& opt,
            const AllocatorElem& ae = {},
            const AllocatorAttr& aa = {},
            const AllocatorText& at = {}
        );
        ~parser();
        //querry
        auto find(const auto& q) const;
        auto flat_find(const auto& q) const;
        void find(auto& result, const auto& q) const;
        void flat_find(auto& result, const auto& q) const;
    private:
        //type erase the allocator
        template<class Alloc, class NodeT>
        void init_all(Alloc& alloc, upp::span<NodeT>& span, std::size_t n);
        void init_all_elem(std::size_t n) final;
        void init_all_attr(std::size_t n) final;
        void init_all_text(std::size_t n) final;
        void emplace_elem(
            node::element_node* p,
            const GumboNode& n,
            std::size_t& oelem,
            std::size_t& oattr,
            std::size_t& otext
        ) final;
        void emplace_attr(node::attribute_node* p, const GumboAttribute& a) final;
        void emplace_text(node::text_node* p, const GumboNode& n) final;
    private:
        AllocatorElem _alloc_elem;
        AllocatorAttr _alloc_attr;
        AllocatorText _alloc_text;
    };
}

#include "parser.ipp"

