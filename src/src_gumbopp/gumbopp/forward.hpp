#pragma once

#include <deque>

#include <gumbo.h>

//typedef struct GumboInternalNode GumboNode;
//typedef struct GumboInternalDocument GumboDocument;
//typedef struct GumboInternalElement GumboElement;
//typedef struct GumboInternalText GumboText;
//typedef struct GumboInternalAttribute GumboAttribute;

//typedef struct GumboInternalOptions GumboOptions;
//typedef struct GumboInternalOutput GumboOutput;
//typedef struct GumboInternalVector GumboVector;
//typedef struct GumboInternalStringPiece GumboStringPiece;

namespace gumbopp
{
    template<class T>
    using query_result = std::deque<std::reference_wrapper<const T>>;

    template
    <
        class AllocatorElem,
        class AllocatorText,
        class AllocatorAttr
        >
    class parser;
}

namespace gumbopp::detail
{
    class parser;
}

namespace gumbopp::node
{
    class node;
    class document_node;
    class element_node;
    class text_node;
    class attribute_node;
}

namespace gumbopp::query_node
{
    class element_query_builder;
    class text_query_builder;
    class attribute_query_builder;
}
