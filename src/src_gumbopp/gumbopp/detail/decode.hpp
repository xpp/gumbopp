﻿#pragma once

#include <string_view>

#include <boost/iterator/transform_iterator.hpp>

#include <gumbo.h>

#include <utilitypp/range/iterator_range.hpp>
#include <utilitypp/exception/throw_if/throw_if_null.hpp>

#include "../forward.hpp"

namespace gumbopp
{

    template <class T>
    const T* decode_and_check(const void* v)
    {
        upp_throw_if_null(std::invalid_argument, v);
        return static_cast<const T*>(v);
    }

    template <class T>
    auto decode(const GumboVector& v)
    {
        return upp::iterator_range
        {
            boost::iterators::make_transform_iterator(v.data,            &decode_and_check<T>),
            boost::iterators::make_transform_iterator(v.data + v.length, &decode_and_check<T>)
        };
    }
    struct child_counts
    {
        std::uint64_t n_elements = 0;
        std::uint64_t n_texts = 0;
    };
    child_counts count_children(const GumboVector& v);

    std::string_view decode(const GumboStringPiece& p);
}
