#include <gumbo.h>

#include <utilitypp/exception/throw_if.hpp>

#include "decode.hpp"
#include "../node/base.hpp"

namespace gumbopp
{
    std::string_view decode(const GumboStringPiece& p)
    {
        if (!p.data)
        {
            upp_throw_if_not_equal_0(std::invalid_argument, p.length)
                    << "Data is null but length is not";
        }
        return {p.data, p.length};
    }

    child_counts count_children(const GumboVector& v)
    {
        child_counts r;
        for (const auto* c : decode<GumboNode>(v))
        {
            switch (node::node::categorize(*c))
            {
                case node::node_category::document:
                    upp_throw(std::logic_error)
                            << "An element should not have a document as child";
                case node::node_category::text:
                    ++r.n_texts;
                    break;
                case node::node_category::element:
                    ++r.n_elements;
                    break;
            }
        }
        return r;
    }
}
