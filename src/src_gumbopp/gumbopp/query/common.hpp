#pragma once

#include <functional>
#include <deque>
#include <regex>
#include <string>

#include "../forward.hpp"

namespace gumbopp::query_node
{
    using element_query_functor = std::function<bool(const node::element_node&)>;
    using attribute_query_functor = std::function<bool(const node::attribute_node&)>;
    using text_query_functor = std::function<bool(const node::text_node&)>;
}

namespace gumbopp::query_node::detail
{
    template<class Fn, class = void>
    struct functor_type : std::false_type
    {
        using type = void;
        using param = void;
    };

    template<class Fn>
struct functor_type<Fn, std::void_t<decltype(element_query_functor(std::declval<Fn>()))>> : std::true_type
    {
        using type = element_query_functor;
        using param = node::element_node;
    };

    template<class Fn>
struct functor_type<Fn, std::void_t<decltype(attribute_query_functor(std::declval<Fn>()))>> : std::true_type
    {
        using type = attribute_query_functor;
        using param = node::attribute_node;
    };

    template<class Fn>
struct functor_type<Fn, std::void_t<decltype(text_query_functor(std::declval<Fn>()))>> : std::true_type
    {
        using type = text_query_functor;
        using param = node::text_node;
    };
}
namespace gumbopp::query_node
{

    template <class T>
    using functor_t = typename detail::functor_type<T>::type;

    template <class T>
    using functor_param_t = typename detail::functor_type<T>::param;

    template<class T>
    static constexpr bool is_element_query_functor = std::is_same_v<functor_t<T>, element_query_functor>;
    template<class T>
    static constexpr bool is_attribute_query_functor = std::is_same_v<functor_t<T>, attribute_query_functor>;
    template<class T>
    static constexpr bool is_text_query_functor = std::is_same_v<functor_t<T>, text_query_functor>;

    template<class FunctorT>
    FunctorT conjunction(std::vector<FunctorT> fs);
    template<class FunctorT, class...Ts>
    FunctorT conjunction(Ts...ts);

    template<class FunctorT>
    FunctorT disjunction(std::vector<FunctorT> fs);
    template<class FunctorT, class...Ts>
    FunctorT disjunction(Ts...ts);
}

#include "common.ipp"
