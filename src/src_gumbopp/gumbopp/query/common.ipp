#pragma once

#include "common.hpp"

namespace gumbopp::query_node
{
    template<class FunctorT>
    inline FunctorT conjunction(std::vector<FunctorT> fs)
    {
        return [fs = std::move(fs)](const auto & e)
        {
            return std::all_of(fs.begin(), fs.end(), [&e](const auto & p)
            {
                return p(e);
            });
        };
    }
    template<class FunctorT, class...Ts>
    inline FunctorT conjunction(Ts...ts)
    {
        return conjunction(std::vector<FunctorT> {FunctorT(std::move(ts))...});
    }

    template<class FunctorT>
    inline FunctorT disjunction(std::vector<FunctorT> fs)
    {
        return [fs = std::move(fs)](const auto & e)
        {
            return std::any_of(fs.begin(), fs.end(), [&e](const auto & p)
            {
                return p(e);
            });
        };
    }
    template<class FunctorT, class...Ts>
    inline FunctorT disjunction(Ts...ts)
    {
        return disjunction(std::vector<FunctorT> {FunctorT(std::move(ts))...});
    }
}
