#pragma once

#include "../node/element.hpp"
#include "../node/attribute.hpp"
#include "../node/text.hpp"

#include "common.hpp"

namespace gumbopp::query_node
{
    //tags
    auto tag(std::string q);
    auto start_tag(std::string q);
    auto end_tag(std::string q);

    //any attribute
    auto attribute(std::string name);
    auto attribute(std::string name, std::string value);

    auto attribute(std::regex name);
    auto attribute(std::regex name, std::string value);
    auto attribute(std::string name, std::regex value);
    auto attribute(std::regex name, std::regex value);
}

#include "element.ipp"
