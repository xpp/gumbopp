#pragma once

#include "element.hpp"

namespace gumbopp::query_node
{
    //tags
    inline auto tag(std::string q)
    {
        return [q = std::move(q)](const node::element_node & n)
        {
            return n.tag() == q;
        };
    }
    inline auto start_tag(std::string q)
    {
        return [q = std::move(q)](const node::element_node & n)
        {
            return n.start_tag() == q;
        };
    }
    inline auto end_tag(std::string q)
    {
        return [q = std::move(q)](const node::element_node & n)
        {
            return n.end_tag() == q;
        };
    }

    //any attribute
    inline auto attribute(std::string name)
    {
        return [name = std::move(name)](const node::element_node & n)
        {
            return n.has_attribute(name);
        };
    }
    inline auto attribute(std::string name, std::string value)
    {
        return [name = std::move(name), value = std::move(value)](const node::element_node & n)
        {
            return n.has_attribute(name) && n.attribute_value(name) == value;
        };
    }

    inline auto attribute(std::regex name)
    {
        return [name = std::move(name)](const node::element_node & n)
        {
            for (const auto& a : n.attributes())
            {
                if (std::regex_match(n.attribute_value(a.name()), name))
                {
                    return true;
                }
            }
            return false;
        };
    }
    inline auto attribute(std::regex name, std::string value)
    {
        return [name = std::move(name), value = std::move(value)](const node::element_node & n)
        {
            for (const auto& a : n.attributes())
            {
                if (std::regex_match(n.attribute_value(a.name()), name) && a.value() == value)
                {
                    return true;
                }
            }
            return false;
        };
    }
    inline auto attribute(std::string name, std::regex value)
    {
        return [name = std::move(name), value = std::move(value)](const node::element_node & n)
        {
            return std::regex_match(n.attribute_value(name), value);
        };
    }
    inline auto attribute(std::regex name, std::regex value)
    {
        return [name = std::move(name), value = std::move(value)](const node::element_node & n)
        {
            for (const auto& a : n.attributes())
            {
                if (
                    std::regex_match(n.attribute_value(a.name()), name) &&
                    std::regex_match(n.attribute_value(a.value()), value)
                )
                {
                    return true;
                }
            }
            return false;
        };
    }
}
