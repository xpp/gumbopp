#pragma once

#include <utilitypp/span.hpp>

#include "common.hpp"

namespace gumbopp::query_node
{
    template<class T, class ResultT>
    class query_builder_template
    {
    public:
        using reult_t = ResultT;

        query_result<reult_t> find(const node::element_node& n) const;
        void find(query_result<reult_t>& r, const node::element_node& n) const;

        query_result<reult_t> find(const upp::span<node::element_node>& nodes) const;
        void find(query_result<reult_t>& r, const upp::span<node::element_node>& nodes) const;
    protected:
        void visit(query_result<reult_t>& r, const node::element_node& n, std::size_t depth) const;

        std::deque<element_query_functor> _element_selectors;
    };

    class element_query_builder : public query_builder_template<element_query_builder, node::element_node>
    {
        friend class query_builder_template<element_query_builder, node::element_node>;
    public:
        template<class...Ts>
        element_query_builder(Ts...ts);
        template<class...Ts>
        element_query_builder(std::string tagstr, Ts...ts);
        template<class...Ts>
        element_query_builder(std::deque<element_query_functor> es, Ts...ts);

        element_query_builder operator()()&& ;

        template<class...Ts>
        element_query_builder operator()(const std::string& tag, Ts...ts)&& ;
        template<class...Ts>
        element_query_builder operator()(const std::string_view& tag, Ts...ts)&& ;
        template<class...Ts>
        element_query_builder operator()(const char* tag, Ts...ts)&& ;

        template<class T0, class...Ts>
        auto operator()(T0 t0, Ts...ts)&& ;
    private:
        void collect(query_result<node::element_node>& r, const node::element_node& n) const;
    };

    class text_query_builder : public query_builder_template<text_query_builder, node::text_node>
    {
        friend class query_builder_template<text_query_builder, node::text_node>;
    public:
        template<class...Ts>
        text_query_builder(std::deque<element_query_functor> es, Ts...ts);
    private:
        void collect(query_result<node::text_node>& r, const node::element_node& n) const;

        text_query_functor _text_selector;
    };

    class attribute_query_builder : public query_builder_template<attribute_query_builder, node::attribute_node>
    {
        friend class query_builder_template<attribute_query_builder, node::attribute_node>;
    public:
        template<class...Ts>
        attribute_query_builder(std::deque<element_query_functor> es, Ts...ts);
    private:
        void collect(query_result<node::attribute_node>& r, const node::element_node& n) const;

        attribute_query_functor _attribute_selector;
    };

    //declare all relevant instantiations as extern
    extern template class query_builder_template<element_query_builder, node::element_node>;
    extern template class query_builder_template<text_query_builder, node::text_node>;
    extern template class query_builder_template<attribute_query_builder, node::attribute_node>;
}

namespace gumbopp
{
    query_node::element_query_builder query();
    template<class...Ts>
    query_node::element_query_builder query(const std::string& tag, Ts...ts);
    template<class...Ts>
    query_node::element_query_builder query(const std::string_view& tag, Ts...ts);
    template<class...Ts>
    query_node::element_query_builder query(const char* tag, Ts...ts);
    template<class T0, class...Ts>

    auto query(T0 t0, Ts...ts);
}

#include "query.ipp"
