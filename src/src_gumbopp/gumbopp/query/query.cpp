#include "../node.hpp"

#include "query.hpp"

namespace gumbopp::query_node
{
    template<class T, class RT>
    query_result<RT> query_builder_template<T, RT>::find(const node::element_node& n) const
    {
        query_result<RT> result;
        find(result, n);
        return result;
    }
    template<class T, class RT>
    void query_builder_template<T, RT>::find(query_result<RT>& r, const node::element_node& n) const
    {
        visit(r, n, 0);
    }

    template<class T, class RT>
    query_result<RT> query_builder_template<T, RT>::find(const upp::span<node::element_node>& nodes) const
    {
        query_result<RT> result;
        find(result, nodes);
        return result;
    }
    template<class T, class RT>
    void query_builder_template<T, RT>::find(query_result<RT>& r, const upp::span<node::element_node>& nodes) const
    {
        for (const auto& e : nodes)
        {
            find(r, e);
        }
    }


    template<class T, class RT>
    void query_builder_template<T, RT>::visit(query_result<RT>& r, const node::element_node& n, std::size_t depth) const
    {
        if (_element_selectors.empty())
        {
            static_cast<const T*>(this)->collect(r, n);
        }
        else if (_element_selectors.at(depth)(n))
        {
            if (depth + 1 == _element_selectors.size())
            {
                static_cast<const T*>(this)->collect(r, n);
            }
            else
            {
                for (const auto& c : n.child_elements())
                {
                    visit(r, c, depth + 1);
                }
            }
        }
    }

    void element_query_builder::collect(query_result<node::element_node>& r, const node::element_node& n) const
    {
        r.emplace_back(n);
    }

    void text_query_builder::collect(query_result<node::text_node>& r, const node::element_node& n) const
    {
        for (const auto& t : n.child_texts())
        {
            if (_text_selector(t))
            {
                r.emplace_back(t);
            }
        }
    }

    void attribute_query_builder::collect(query_result<node::attribute_node>& r, const node::element_node& n) const
    {
        for (const auto& a : n.attributes())
        {
            if (_attribute_selector(a))
            {
                r.emplace_back(a);
            }
        }
    }
}
