#pragma once

#include "query.hpp"
#include "element.hpp"

namespace gumbopp::query_node
{
    template<class...Ts>
    inline element_query_builder::element_query_builder(Ts...ts)
    {
        _element_selectors.emplace_back(conjunction<element_query_functor>(std::move(ts)...));
    }
    template<class...Ts>
    inline element_query_builder::element_query_builder(std::string tagstr, Ts...ts)
    {
        _element_selectors.emplace_back(
            conjunction<element_query_functor>
            (
                tag(std::move(tagstr)),
                std::move(ts)...
            )
        );
    }
    template<class...Ts>
    inline element_query_builder::element_query_builder(std::deque<element_query_functor> es, Ts...ts)
    {
        _element_selectors = std::move(es);
        _element_selectors.emplace_back(conjunction<element_query_functor>(std::move(ts)...));
    }

    template class query_builder_template<element_query_builder, node::element_node>;
    template class query_builder_template<text_query_builder, node::text_node>;
    template class query_builder_template<attribute_query_builder, node::attribute_node>;

    inline element_query_builder element_query_builder::operator()()&&
    {
        return element_query_builder{std::move(_element_selectors)};
    }
    template<class...Ts>
    inline element_query_builder element_query_builder::operator()(const std::string& t, Ts...ts)&&
    {
        return {std::move(_element_selectors), tag(t), std::move(ts)...};
    }
    template<class...Ts>
    inline element_query_builder element_query_builder::operator()(const std::string_view& t, Ts...ts)&&
    {
        return {std::move(_element_selectors), tag(std::string{t}), std::move(ts)...};
    }
    template<class...Ts>
    inline element_query_builder element_query_builder::operator()(const char* t, Ts...ts)&&
    {
        return {std::move(_element_selectors), tag(t), std::move(ts)...};
    }

    template<class T0, class...Ts>
    inline auto element_query_builder::operator()(T0 t0, Ts...ts)&&
    {
        if constexpr(is_element_query_functor<T0>)
        {
            return element_query_builder{std::move(_element_selectors), std::move(t0), std::move(ts)...};
        }
        else if constexpr(is_attribute_query_functor<T0>)
        {
            return attribute_query_builder{std::move(_element_selectors), std::move(t0), std::move(ts)...};
        }
        else if constexpr(is_text_query_functor<T0>)
        {
            return text_query_builder{std::move(_element_selectors), std::move(t0), std::move(ts)...};
        }
        else
        {
            static_assert(sizeof...(Ts) < 0, "Unknown functor type");
        }
    }

    template<class...Ts>
    inline text_query_builder::text_query_builder(std::deque<element_query_functor> es, Ts...ts) :
        _text_selector{conjunction(std::move(ts)...)}
    {
        _element_selectors = std::move(es);
    }
    template<class...Ts>
    attribute_query_builder::attribute_query_builder(std::deque<element_query_functor> es, Ts...ts) :
        _attribute_selector{conjunction(std::move(ts)...)}
    {
        _element_selectors = std::move(es);
    }
}

namespace gumbopp
{
    inline query_node::element_query_builder query()
    {
        return {};
    }
    template<class...Ts>
    inline query_node::element_query_builder query(const std::string& tag, Ts...ts)
    {
        return {tag, std::move(ts)...};
    }
    template<class...Ts>
    inline query_node::element_query_builder query(const std::string_view& tag, Ts...ts)
    {
        return {std::string{tag}, std::move(ts)...};
    }
    template<class...Ts>
    inline query_node::element_query_builder query(const char* tag, Ts...ts)
    {
        return {std::string{tag}, std::move(ts)...};
    }

    template<class T0, class...Ts>
    inline auto query(T0 t0, Ts...ts)
    {
        if constexpr(query_node::is_element_query_functor<T0>)
        {
            return query_node::element_query_builder{std::move(t0), std::move(ts)...};
        }
        else if constexpr(query_node::is_attribute_query_functor<T0>)
        {
            return query_node::attribute_query_builder{{}, std::move(t0), std::move(ts)...};
        }
        else if constexpr(query_node::is_text_query_functor<T0>)
        {
            return query_node::text_query_builder{{}, std::move(t0), std::move(ts)...};
        }
        else
        {
            static_assert(sizeof...(Ts) < 0, "Unknown functor type");
        }
    }
}
