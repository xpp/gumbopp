#include <cstring>
#include <functional>

#include <gumbo.h>

#include "detail/decode.hpp"
#include "parser.hpp"

namespace gumbopp::detail
{
    void parser::init(const char* buffer)
    {
        init(buffer, kGumboDefaultOptions);
    }

    void parser::init(const char* buffer, const GumboOptions& opt)
    {
        _options = opt;
        _output = gumbo_parse_with_options(&gumbo_options(), buffer, std::strlen(buffer));
        //scan
        std::size_t n_elements = 0;
        std::size_t n_texts = 0;
        std::size_t n_attributes = 0;
        {
            std::uint64_t n_documents = 0; //added to check there is only one document
            std::function<void(const GumboNode*)> visit = [&](const GumboNode * n)
            {
                upp_throw_if_null(std::logic_error, n)
                        << "A node created by the gumbo-parser is null";
                switch (node::node::categorize(*n))
                {
                    case node::node_category::text:
                    {
                        ++n_texts;
                    }
                    break;
                    case node::node_category::document:
                    {
                        ++n_documents;
                        for (const auto* c : decode<GumboNode>(n->v.document.children))
                        {
                            visit(c);
                        }
                    }
                    break;
                    case node::node_category::element:
                    {
                        ++n_elements;
                        n_attributes += n->v.element.attributes.length;
                        for (const auto* c : decode<GumboNode>(n->v.element.children))
                        {
                            visit(c);
                        }
                    }
                    break;
                }
            };
            visit(gumbo_output().document);
            upp_throw_if_not_equal_1(std::logic_error, n_documents)
                    << "The gumbo tree contained multiple document nodes";
        }
        //allocate storage
        {
            init_all_elem(n_elements);
            init_all_text(n_texts);
            init_all_attr(n_attributes);
        }
        //build tree
        _document.init(*_output->document, *this);
        for (const auto& e : _document.child_elements())
        {
            if (&e.gumbo_node() == gumbo_output().root)
            {
                _root = &e;
            }
        }
        upp_throw_if_null(std::logic_error, _root)
                << "The root node is not a child of document!";
    }

    parser::~parser()
    {
        if (_output)
        {
            gumbo_destroy_output(&gumbo_options(), _output);
        }
    }

    const GumboOptions& parser::gumbo_options() const
    {
        return std::any_cast<const GumboOptions&>(_options);
    }

    const GumboOutput& parser::gumbo_output() const
    {
        return *_output;
    }

    const node::element_node& parser::root() const
    {
        return *_root;
    }

    const node::document_node& parser::document() const
    {
        return _document;
    }

    const upp::span<node::element_node>& parser::all_elements() const
    {
        return _all_elem;
    }

    const upp::span<node::text_node>& parser::all_texts() const
    {
        return _all_text;
    }

    const upp::span<node::attribute_node>& parser::all_attributes() const
    {
        return _all_attr;
    }
}
