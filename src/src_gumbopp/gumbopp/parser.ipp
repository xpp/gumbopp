#pragma once

#include <any>
#include <memory>
#include <cstring>

#include <utilitypp/memory/compare.hpp>

#include "node.hpp"

namespace gumbopp
{
    //ctor
    template<class E, class A, class T>
    inline parser<E, A, T>::parser(
        const char* buffer,
        const E& ae,
        const A& aa,
        const T& at
    ) :
        _alloc_elem{ae},
        _alloc_attr{aa},
        _alloc_text{at}
    {
        init(buffer);
    }

    template<class E, class A, class T>
    inline parser<E, A, T>::parser(
        const char* buffer,
        const GumboOptions& opt,
        const E& ae,
        const A& aa,
        const T& at
    ) :
        _alloc_elem{ae},
        _alloc_attr{aa},
        _alloc_text{at}
    {
        init(buffer, opt);
    }

    //dtor
    template<class E, class A, class T>
    inline parser<E, A, T>::~parser()
    {
        if (_all_elem.size())
        {
            _alloc_elem.deallocate(_all_elem.begin(), _all_elem.size());
        }
        if (_all_attr.size())
        {
            _alloc_attr.deallocate(_all_attr.begin(), _all_attr.size());
        }
        if (_all_text.size())
        {
            _alloc_text.deallocate(_all_text.begin(), _all_text.size());
        }
    }

    //querry
    template<class E, class A, class T>
    inline auto parser<E, A, T>::find(const auto& q) const
    {
        return _document.find(q);
    }
    template<class E, class A, class T>
    inline auto parser<E, A, T>::flat_find(const auto& q) const
    {
        return q.find(_all_elem);
    }

    template<class E, class A, class T>
    inline void parser<E, A, T>::find(auto& result, const auto& q) const
    {
        _document.find(result, q);
    }
    template<class E, class A, class T>
    inline void parser<E, A, T>::flat_find(auto& result, const auto& q) const
    {
        return q.find(result, _all_elem);
    }

    //inits
    template<class E, class A, class T>
    template<class Alloc, class NodeT>
    inline void parser<E, A, T>::init_all(Alloc& alloc, upp::span<NodeT>& span, std::size_t n)
    {
        if (!n)
        {
            return;
        }
        auto ptr = alloc.allocate(n);
        span = upp::span{ptr, n};
        //ok since the memory is only allocated
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
        std::memset(ptr, 0, n * sizeof(NodeT));
#pragma GCC diagnostic pop
    }

    template<class E, class A, class T>
    inline void parser<E, A, T>:: init_all_elem(std::size_t n)
    {
        init_all(_alloc_elem, _all_elem, n);
    }

    template<class E, class A, class T>
    inline void parser<E, A, T>:: init_all_attr(std::size_t n)
    {
        init_all(_alloc_attr, _all_attr, n);
    }

    template<class E, class A, class T>
    inline void parser<E, A, T>:: init_all_text(std::size_t n)
    {
        init_all(_alloc_text, _all_text, n);
    }

    //emplace
    template<class E, class A, class T>
    inline void parser<E, A, T>:: emplace_elem(
        node::element_node* p,
        const GumboNode& n,
        std::size_t& oelem,
        std::size_t& oattr,
        std::size_t& otext
    )
    {
        upp_throw_if(std::logic_error, _all_elem.empty());
        upp_throw_if_less(std::logic_error, p, _all_elem.begin());
        upp_throw_if_not_less(std::logic_error, p, _all_elem.end());
        upp_throw_if_not(std::logic_error, upp::memcmp_bytes(p, 0))
                << "Entry at " << p << " is already not initialized";
        _alloc_elem.construct(p, n, *this, oelem, oattr, otext);
    }

    template<class E, class A, class T>
    inline void parser<E, A, T>:: emplace_attr(node::attribute_node* p, const GumboAttribute& a)
    {
        upp_throw_if(std::logic_error, _all_attr.empty());
        upp_throw_if_less(std::logic_error, p, _all_attr.begin());
        upp_throw_if_not_less(std::logic_error, p, _all_attr.end());
        upp_throw_if_not(std::logic_error, upp::memcmp_bytes(p, 0))
                << "Entry at " << p << " is already not initialized";
        _alloc_attr.construct(p, a);
    }

    template<class E, class A, class T>
    inline void parser<E, A, T>:: emplace_text(node::text_node* p, const GumboNode& n)
    {
        upp_throw_if(std::logic_error, _all_text.empty());
        upp_throw_if_less(std::logic_error, p, _all_text.begin());
        upp_throw_if_not_less(std::logic_error, p, _all_text.end());
        upp_throw_if_not(std::logic_error, upp::memcmp_bytes(p, 0))
                << "Entry at " << p << " is already not initialized";
        _alloc_text.construct(p, n);
    }


}
