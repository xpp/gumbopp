#pragma once


namespace gumbopp::node
{
    class error_node
    {
    public:
        upp_default_cexpr_copy(error_node);

        //GumboError* _gerror{nullptr};
    };
    static_assert(std::is_trivially_destructible_v<error_node>);
    static_assert(std::is_trivially_copy_constructible_v<error_node>);
    static_assert(std::is_trivially_copy_assignable_v<error_node>);
}
