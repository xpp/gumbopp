#pragma once

#include <utilitypp/exception/throw_if.hpp>
#include <utilitypp/generate_code/default_ctor_and_assign.hpp>

namespace gumbopp::node
{
    enum class node_category
    {
        document,
        element,
        text
    };
    enum class element_category
    {
        element_node,
        template_node
    };
    enum class text_category
    {
        text_node,
        cdata_node,
        comment_node,
        whitespace_node
    };
    //    template<node_category C>
    //    struct node_category_type_mapper;
    //    template<>
    //    struct node_category_type_mapper<node_category::document> : std::id;
    ///TODO

    class node
    {
    public:
        static node_category categorize(const GumboNode& n);

        node_category category() const;

        const GumboNode& gumbo_node() const;
        const GumboNode* gumbo_node_parent() const;

        //        template<class T>
        //        T& cast();
        //        template<class T>
        //        const T& cast() const;

    protected:
        template<node_category Cat>
        void init(const GumboNode& n)
        {
            _gnode = &n;
            upp_throw_if_not(
                std::invalid_argument,
                categorize(n) == Cat
            );
        }

        const GumboNode* _gnode{nullptr};
    };
    static_assert(std::is_trivially_destructible_v<node>);
    static_assert(std::is_trivially_copy_constructible_v<node>);
    static_assert(std::is_trivially_copy_assignable_v<node>);
    //    template<class T>
    //    const T& node::cast() const = delete;
    //    template<>
    //    element& node::cast()
    //    {
    //        upp_throw_if_equal(std::logic_error, node_category::element, category());
    //        return *static_cast<element*>(this);
    //    }
}
