#include <gumbo.h>

#include "attribute.hpp"

namespace gumbopp::node
{
    attribute_node::attribute_node(const GumboAttribute& attr) :
        _attribute{&attr}
    {}

    const char* attribute_node::name() const
    {
        return _attribute->name;
    }

    const char* attribute_node::value() const
    {
        return _attribute->value;
    }

    const GumboAttribute& attribute_node::gumbo_attribute() const
    {
        return *_attribute;
    }

    bool operator<(const attribute_node& attr, const std::string_view& q)
    {
        return std::string_view{attr.name()} < q;
    }

    bool operator<(const std::string_view& q, const attribute_node& attr)
    {
        return q < std::string_view{attr.name()};
    }

    bool operator<(const attribute_node& l, const attribute_node& r)
    {
        return std::string_view{l.name()} < std::string_view{r.name()};
    }

    bool operator==(const attribute_node& attr, const std::string_view& q)
    {
        return std::string_view{attr.name()} == q;
    }

    bool operator==(const std::string_view& q, const attribute_node& attr)
    {
        return q == std::string_view{attr.name()};
    }

    bool operator==(const attribute_node& l, const attribute_node& r)
    {
        return std::string_view{l.name()} == std::string_view{r.name()};
    }
}
