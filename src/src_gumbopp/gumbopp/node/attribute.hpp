#pragma once

#include <string_view>

#include <utilitypp/generate_code/default_ctor_and_assign.hpp>

#include "../forward.hpp"

namespace gumbopp::node
{
    class attribute_node
    {
    public:
        upp_default_cexpr_copy(attribute_node);

        attribute_node(const GumboAttribute& attr);

        const char* name() const;
        const char* value() const;
        const GumboAttribute& gumbo_attribute() const;
    private:
        const GumboAttribute* _attribute{nullptr};
        ///TODO  GumboAttributeNamespaceEnum
    };
    static_assert(std::is_trivially_destructible_v<attribute_node>);
    static_assert(std::is_trivially_copy_constructible_v<attribute_node>);
    static_assert(std::is_trivially_copy_assignable_v<attribute_node>);

    bool operator<(const attribute_node& attr, const std::string_view& q);
    bool operator<(const std::string_view& q, const attribute_node& attr);
    bool operator<(const attribute_node& l, const attribute_node& r);
    bool operator==(const attribute_node& attr, const std::string_view& q);
    bool operator==(const std::string_view& q, const attribute_node& attr);
    bool operator==(const attribute_node& l, const attribute_node& r);
}
