#include <gumbo.h>

#include "base.hpp"

namespace gumbopp::node
{
    node_category node::categorize(const GumboNode& n)
    {
        switch (n.type)
        {
            case GUMBO_NODE_DOCUMENT :
                return node_category::document;

            case GUMBO_NODE_ELEMENT :
            case GUMBO_NODE_TEMPLATE :
                return node_category::element;

            case GUMBO_NODE_TEXT :
            case GUMBO_NODE_CDATA :
            case GUMBO_NODE_COMMENT :
            case GUMBO_NODE_WHITESPACE :
                return node_category::text;
        }
        upp_throw(std::invalid_argument)
                << "unknown gumbo node type " << n.type;
    }

    node_category node::category() const
    {
        return categorize(gumbo_node());
    }

    const GumboNode& node::gumbo_node() const
    {
        return *_gnode;
    }

    const GumboNode* node::gumbo_node_parent() const
    {
        return _gnode->parent;
    }
}
