#pragma once

#include <utilitypp/span.hpp>

#include "element.hpp"
#include "text.hpp"

namespace gumbopp::node
{
    class document_node : public node
    {
        friend class gumbopp::detail::parser;
    public:
        upp_default_cexpr_copy(document_node);

        const GumboDocument& gumbo_document() const;

        const upp::span<element_node>& child_elements() const;
        const upp::span<text_node   >& child_texts()    const;

        const upp::span<element_node  >& all_sub_elements()   const;
        const upp::span<attribute_node>& all_sub_attributes() const;
        const upp::span<text_node     >& all_sub_texts()      const;

        const char* name() const;
        const char* public_identifier() const;
        const char* system_identifier() const;

        query_result<element_node  > find(const query_node::element_query_builder&   query) const;
        query_result<text_node     > find(const query_node::text_query_builder&      query) const;
        query_result<attribute_node> find(const query_node::attribute_query_builder& query) const;

        void find(query_result<element_node  >& result, const query_node::element_query_builder&   query) const;
        void find(query_result<text_node     >& result, const query_node::text_query_builder&      query) const;
        void find(query_result<attribute_node>& result, const query_node::attribute_query_builder& query) const;

        query_result<element_node  > flat_find(const query_node::element_query_builder&   query) const;
        query_result<text_node     > flat_find(const query_node::text_query_builder&      query) const;
        query_result<attribute_node> flat_find(const query_node::attribute_query_builder& query) const;

        void flat_find(query_result<element_node  >& result, const query_node::element_query_builder&   query) const;
        void flat_find(query_result<text_node     >& result, const query_node::text_query_builder&      query) const;
        void flat_find(query_result<attribute_node>& result, const query_node::attribute_query_builder& query) const;
    private:
        document_node() = default;
        void init(const GumboNode& n, detail::parser& p);

        upp::span<element_node  > _elems;
        upp::span<text_node     > _texts;

        upp::span<element_node  > _all_sub_elems;
        upp::span<text_node     > _all_sub_texts;
        upp::span<attribute_node> _all_sub_attrs;
    };
    static_assert(std::is_trivially_destructible_v<document_node>);
    static_assert(std::is_trivially_copy_constructible_v<document_node>);
    static_assert(std::is_trivially_copy_assignable_v<document_node>);
}
