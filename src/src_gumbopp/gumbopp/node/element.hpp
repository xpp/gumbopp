#pragma once

#include <vector>

#include <utilitypp/span.hpp>

#include "base.hpp"
#include "attribute.hpp"

namespace gumbopp::node
{
    class element_node : public node
    {
    public:
        upp_default_cexpr_copy(element_node);

        element_node(
            const GumboNode& n,
            detail::parser& p,
            std::size_t& oelem,
            std::size_t& oattr,
            std::size_t& otext
        );
        const GumboElement& gumbo_element() const;

        std::string_view start_tag() const;
        std::string_view tag() const;
        std::string_view end_tag() const;

        const upp::span<element_node  >& child_elements() const;
        const upp::span<attribute_node>& attributes()     const;
        const upp::span<text_node     >& child_texts()    const;

        const upp::span<element_node  >& all_sub_elements()   const;
        const upp::span<attribute_node>& all_sub_attributes() const;
        const upp::span<text_node     >& all_sub_texts()      const;

        bool has_attribute(const std::string_view& name) const;
        const attribute_node& attribute(const std::string_view& name) const;
        const char* attribute_value(const std::string_view& name) const;

        query_result<element_node  > find(const query_node::element_query_builder&   query) const;
        query_result<text_node     > find(const query_node::text_query_builder&      query) const;
        query_result<attribute_node> find(const query_node::attribute_query_builder& query) const;

        void find(query_result<element_node  >& result, const query_node::element_query_builder&   query) const;
        void find(query_result<text_node     >& result, const query_node::text_query_builder&      query) const;
        void find(query_result<attribute_node>& result, const query_node::attribute_query_builder& query) const;

        query_result<element_node  > flat_find(const query_node::element_query_builder&   query) const;
        query_result<text_node     > flat_find(const query_node::text_query_builder&      query) const;
        query_result<attribute_node> flat_find(const query_node::attribute_query_builder& query) const;

        void flat_find(query_result<element_node  >& result, const query_node::element_query_builder&   query) const;
        void flat_find(query_result<text_node     >& result, const query_node::text_query_builder&      query) const;
        void flat_find(query_result<attribute_node>& result, const query_node::attribute_query_builder& query) const;
    private:
        upp::span<element_node  > _elems;
        upp::span<attribute_node> _attrs;
        upp::span<text_node     > _texts;

        upp::span<element_node  > _all_sub_elems;
        upp::span<attribute_node> _all_sub_attrs;
        upp::span<text_node     > _all_sub_texts;
        ///TODO GumboTag
    };
    static_assert(std::is_trivially_destructible_v<element_node>);
    static_assert(std::is_trivially_copy_constructible_v<element_node>);
    static_assert(std::is_trivially_copy_assignable_v<element_node>);

}
