#include <gumbo.h>

#include "text.hpp"

namespace gumbopp::node
{
    text_node::text_node(const GumboNode& n)
    {
        init<node_category::text>(n);
    }

    const GumboText& text_node::gumbo_text() const
    {
        return _gnode->v.text;
    }

    const char* text_node::c_str() const
    {
        return gumbo_text().text;
    }
}
