#include <gumbo.h>

#include "../query.hpp"
#include "../detail/decode.hpp"
#include "../parser.hpp"
#include "element.hpp"
#include "text.hpp"


//functions common between element and document
namespace gumbopp::node
{
    //access direct members
    const upp::span<element_node>& element_node::child_elements() const
    {
        return _elems;
    }
    const upp::span<attribute_node>& element_node::attributes() const
    {
        return _attrs;
    }
    const upp::span<text_node>& element_node::child_texts() const
    {
        return _texts;
    }

    //access all sub members
    const upp::span<element_node  >& element_node::all_sub_elements()   const
    {
        return _all_sub_elems;
    }
    const upp::span<attribute_node>& element_node::all_sub_attributes() const
    {
        return _all_sub_attrs;
    }
    const upp::span<text_node     >& element_node::all_sub_texts()      const
    {
        return _all_sub_texts;
    }

    //query
    query_result<element_node  > element_node::find(const query_node::element_query_builder&   query) const
    {
        return query.find(*this);
    }
    query_result<text_node     > element_node::find(const query_node::text_query_builder&      query) const
    {
        return query.find(*this);
    }
    query_result<attribute_node> element_node::find(const query_node::attribute_query_builder& query) const
    {
        return query.find(*this);
    }

    void element_node::find(query_result<element_node  >& result, const query_node::element_query_builder&   query) const
    {
        query.find(result, *this);
    }
    void element_node::find(query_result<text_node     >& result, const query_node::text_query_builder&      query) const
    {
        query.find(result, *this);
    }
    void element_node::find(query_result<attribute_node>& result, const query_node::attribute_query_builder& query) const
    {
        query.find(result, *this);
    }

    //flat query
    query_result<element_node  > element_node::flat_find(const query_node::element_query_builder&   query) const
    {
        return query.find(_all_sub_elems);
    }
    query_result<text_node     > element_node::flat_find(const query_node::text_query_builder&      query) const
    {
        return query.find(_all_sub_elems);
    }
    query_result<attribute_node> element_node::flat_find(const query_node::attribute_query_builder& query) const
    {
        return query.find(_all_sub_elems);
    }

    void element_node::flat_find(query_result<element_node  >& result, const query_node::element_query_builder&   query) const
    {
        query.find(result, _all_sub_elems);
    }
    void element_node::flat_find(query_result<text_node     >& result, const query_node::text_query_builder&      query) const
    {
        query.find(result, _all_sub_elems);
    }
    void element_node::flat_find(query_result<attribute_node>& result, const query_node::attribute_query_builder& query) const
    {
        query.find(result, _all_sub_elems);
    }
}

//other functions
namespace gumbopp::node
{
    element_node::element_node(
        const GumboNode& n,
        detail::parser& p,
        std::size_t& oelem,
        std::size_t& oattr,
        std::size_t& otext
    )
    {
        init<node_category::element>(n);
        const auto counts = count_children(gumbo_element().children);
        const auto n_elem = counts.n_elements;
        const auto n_attr = gumbo_element().attributes.length;
        const auto n_text = counts.n_texts;
        //get chunks of storage
        {
            _elems = upp::span{p._all_elem.begin() + oelem, n_elem};
            _attrs = upp::span{p._all_attr.begin() + oattr, n_attr};
            _texts = upp::span{p._all_text.begin() + otext, n_text};
        }
        //these are the offsets passed to ctors of element
        //they are incremented and used to track which chunks of storage are used
        //increase them now
        {
            oelem += n_elem;
            oattr += n_attr;
            otext += n_text;
        }
        //build children
        {
            //for this we need local counts to determine the offset in the spans
            std::size_t current_oelem = 0;
            std::size_t current_oattr = 0;
            std::size_t current_otext = 0;
            for (const auto attr : decode<GumboAttribute>(gumbo_element().attributes))
            {
                upp_throw_if_not_less(std::logic_error, current_oattr, _attrs.size());
                p.emplace_attr(_attrs.begin() + current_oattr, *attr);
                ++current_oattr;
                ///TODO add to some lookup structure
            }
            for (const auto child : decode<GumboNode>(gumbo_element().children))
            {
                const auto cat = node::categorize(*child);
                if (cat == node_category::text)
                {
                    upp_throw_if_not_less(std::logic_error, current_otext, _texts.size());
                    p.emplace_text(_texts.begin() + current_otext, *child);
                    ++current_otext;
                }
                else if (cat == node_category::element)
                {
                    upp_throw_if_not_less(std::logic_error, current_oelem, _elems.size());
                    p.emplace_elem(
                        _elems.begin() + current_oelem,
                        *child,
                        oelem,
                        oattr,
                        otext
                    );
                    ++current_oelem;
                }
                else
                {
                    upp_throw(std::invalid_argument)
                            << "element has document as child";
                }
            }
            upp_throw_if_not_equal(std::logic_error, current_oelem, _elems.size());
            upp_throw_if_not_equal(std::logic_error, current_oattr, _attrs.size());
            upp_throw_if_not_equal(std::logic_error, current_otext, _texts.size());
        }
        //get chunks of storage for all subs
        {
            _all_sub_elems = upp::span{_elems.begin(), p._all_elem.begin() + oelem};
            _all_sub_attrs = upp::span{_attrs.begin(), p._all_attr.begin() + oattr};
            _all_sub_texts = upp::span{_texts.begin(), p._all_text.begin() + otext};
        }
    }

    const GumboElement& element_node::gumbo_element() const
    {
        return _gnode->v.element;
    }

    std::string_view element_node::start_tag() const
    {
        return decode(gumbo_element().original_tag);
    }

    std::string_view element_node::tag() const
    {
        if (!end_tag().empty())
        {
            auto result = end_tag();
            result.remove_prefix(2);
            result.remove_suffix(1);
            return result;
        }
        //tag is of form <TAGNAME( .+)?/>
        auto result = start_tag();
        result.remove_prefix(1);
        const auto p = result.find_first_of(" ");
        if (p == std::string_view::npos)
        {
            result.remove_suffix(2);
            return result;
        }
        return {result.data(), p};
    }

    std::string_view element_node::end_tag() const
    {
        return decode(gumbo_element().original_end_tag);
    }

    bool element_node::has_attribute(const std::string_view& name) const
    {
        return std::find(_attrs.begin(), _attrs.end(), name) != _attrs.end();
    }

    const attribute_node& element_node::attribute(const std::string_view& name) const
    {
        const auto it = std::find(_attrs.begin(), _attrs.end(), name);
        const bool has_attribute = it != _attrs.end();
        upp_throw_if_not(std::invalid_argument, has_attribute)
                << "No attribute with name " << name << " exists";
        return *it;
    }

    const char* element_node::attribute_value(const std::string_view& name) const
    {
        return attribute(name).value();
    }
}
