#pragma once

#include "base.hpp"

namespace gumbopp::node
{
    class text_node : public node
    {
    public:
        upp_default_cexpr_copy(text_node);

        text_node(const GumboNode& n);

        const GumboText& gumbo_text() const;

        const char* c_str() const;
    };
    static_assert(std::is_trivially_destructible_v<text_node>);
    static_assert(std::is_trivially_copy_constructible_v<text_node>);
    static_assert(std::is_trivially_copy_assignable_v<text_node>);
}
