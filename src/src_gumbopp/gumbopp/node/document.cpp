#include <gumbo.h>

#include <utilitypp/memory/compare.hpp>

#include "../detail/decode.hpp"
#include "../parser.hpp"
#include "../query.hpp"
#include "document.hpp"

//functions common between element and document
namespace gumbopp::node
{
    //access direct members
    const upp::span<element_node>& document_node::child_elements() const
    {
        return _elems;
    }
    /*no attributes
     *{
     *
     *} */
    const upp::span<text_node>& document_node::child_texts() const
    {
        return _texts;
    }

    //access all sub members
    const upp::span<element_node  >& document_node::all_sub_elements()   const
    {
        return _all_sub_elems;
    }
    const upp::span<attribute_node>& document_node::all_sub_attributes() const
    {
        return _all_sub_attrs;
    }
    const upp::span<text_node     >& document_node::all_sub_texts()      const
    {
        return _all_sub_texts;
    }

    //query
    query_result<element_node  > document_node::find(const query_node::element_query_builder&   query) const
    {
        return query.find(_elems);
    }
    query_result<text_node     > document_node::find(const query_node::text_query_builder&      query) const
    {
        return query.find(_elems);
    }
    query_result<attribute_node> document_node::find(const query_node::attribute_query_builder& query) const
    {
        return query.find(_elems);
    }

    void document_node::find(query_result<element_node  >& result, const query_node::element_query_builder&   query) const
    {
        query.find(result, _elems);
    }
    void document_node::find(query_result<text_node     >& result, const query_node::text_query_builder&      query) const
    {
        query.find(result, _elems);
    }
    void document_node::find(query_result<attribute_node>& result, const query_node::attribute_query_builder& query) const
    {
        query.find(result, _elems);
    }

    //flat query
    query_result<element_node  > document_node::flat_find(const query_node::element_query_builder&   query) const
    {
        return query.find(_all_sub_elems);
    }
    query_result<text_node     > document_node::flat_find(const query_node::text_query_builder&      query) const
    {
        return query.find(_all_sub_elems);
    }
    query_result<attribute_node> document_node::flat_find(const query_node::attribute_query_builder& query) const
    {
        return query.find(_all_sub_elems);
    }

    void document_node::flat_find(query_result<element_node  >& result, const query_node::element_query_builder&   query) const
    {
        query.find(result, _all_sub_elems);
    }
    void document_node::flat_find(query_result<text_node     >& result, const query_node::text_query_builder&      query) const
    {
        query.find(result, _all_sub_elems);
    }
    void document_node::flat_find(query_result<attribute_node>& result, const query_node::attribute_query_builder& query) const
    {
        query.find(result, _all_sub_elems);
    }
}

//other functions
namespace gumbopp::node
{
    void document_node::init(const GumboNode& n, detail::parser& p)
    {
        node::init<node_category::document>(n);
        const auto counts = count_children(gumbo_document().children);
        //these are the offsets passed to ctors of element
        //they are incremented and used to track which chunks of storage are used
        std::size_t oelem = counts.n_elements;
        std::size_t oattr = 0;
        std::size_t otext = counts.n_texts;
        //get chunks of storage
        _elems = upp::span{p._all_elem.begin(), oelem};
        _texts = upp::span{p._all_text.begin(), otext};
        const auto attribptr = p._all_attr.begin();
        //build children
        {
            //for this we need local counts to determine the offset in the spans
            std::size_t current_oelem = 0;
            std::size_t current_otext = 0;
            for (const auto child : decode<GumboNode>(gumbo_document().children))
            {
                const auto cat = node::categorize(*child);
                if (cat == node_category::text)
                {
                    upp_throw_if_not_less(std::logic_error, current_otext, _texts.size());
                    p.emplace_text(_texts.begin() + current_otext, *child);
                    ++current_otext;
                }
                else if (cat == node_category::element)
                {
                    upp_throw_if_not_less(std::logic_error, current_oelem, _elems.size());
                    p.emplace_elem(
                        _elems.begin() + current_oelem,
                        *child,
                        oelem,
                        oattr,
                        otext
                    );
                    ++current_oelem;
                }
                else
                {
                    upp_throw(std::logic_error)
                            << "element has document as child";
                }
            }
            upp_throw_if_not_equal(std::logic_error, current_oelem, _elems.size());
            upp_throw_if_not_equal(std::logic_error, current_otext, _texts.size());
        }
        //check all offsets are correct and all memory was used
        {
            upp_throw_if_not_equal(std::logic_error, p._all_elem.size(), oelem)
                    << "Not all elements were constructed! Something is seriously wrong";
            upp_throw_if_not_equal(std::logic_error, p._all_attr.size(), oattr)
                    << "Not all attributes were constructed! Something is seriously wrong";
            upp_throw_if_not_equal(std::logic_error, p._all_text.size(), otext)
                    << "Not all texts were constructed! Something is seriously wrong";
            const auto check_no_idx_null = [](const auto & span, const auto name)
            {
                for (std::size_t i = 0; i < span.size(); ++i)
                {
                    upp_throw_if(
                        std::logic_error,
                        upp::memcmp_bytes(span.begin() + i, 0)
                    ) << "Entry " << i << " of " << name << " is not initialized";
                }
            };
            check_no_idx_null(p._all_elem, "all elements");
            check_no_idx_null(p._all_attr, "all attributes");
            check_no_idx_null(p._all_text, "all texts");
        }
        //get chunks of storage for all subs
        {
            _all_sub_elems = upp::span{_elems.begin(), p._all_elem.begin() + oelem};
            _all_sub_attrs = upp::span{attribptr, p._all_attr.begin() + oattr};
            _all_sub_texts = upp::span{_texts.begin(), p._all_text.begin() + otext};
        }
    }

    const GumboDocument& document_node::gumbo_document() const
    {
        return _gnode->v.document;
    }

    const char* document_node::name() const
    {
        return gumbo_document().name;
    }

    const char* document_node::public_identifier() const
    {
        return gumbo_document().public_identifier;
    }

    const char* document_node::system_identifier() const
    {
        return gumbo_document().system_identifier;
    }
}
