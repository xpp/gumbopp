#pragma once

// This file is generated!

#include "node/attribute.hpp"
#include "node/base.hpp"
#include "node/document.hpp"
#include "node/element.hpp"
#include "node/error.hpp"
#include "node/text.hpp"
