#define BOOST_TEST_MODULE gumbopp

#include <boost/test/included/unit_test.hpp>

#include <gumbopp/parser.hpp>
#include <gumbopp/query.hpp>

#include <chrono>

BOOST_AUTO_TEST_CASE(find)
{
    const auto print_t = [start = std::chrono::high_resolution_clock::now()](const auto & tag)
    {
        const double now = (std::chrono::high_resolution_clock::now() - start).count();
        std::cout << '[' << now / 1'000 << " us] " << tag << std::endl;
    };

    print_t("start");
    gumbopp::parser p
    {
        R"(
            <html>
                <head>
                    <title>titletext</title>
                    <meta charset="UTF-8" />
                </head>
                <body class="classvalue">
                <div id="0">
                    <div id="1">
                        <div id="2">
                            <h5>
                                <a href="/">
                                    hreftext
                                </a>
                            </h5>
                        </div>
                    </div>
                </div>
                <div id="0">
                    <div id="1"/>
                    <div id="2"/>
                </div>
                </body>
            </html>
        )"
    };
    print_t("parsed");

    using namespace gumbopp::query_node;
    BOOST_CHECK_EQUAL(
        p.find
        (
            gumbopp::query(tag("html"))
        ).size(),
        1
    );
    BOOST_CHECK_EQUAL(
        p.find
        (
            gumbopp::query()
        ).size(),
        1
    );

    BOOST_CHECK_EQUAL(
        p.find(
            gumbopp::query(tag("html"))()
        ).size(),
        2
    );

    BOOST_CHECK_EQUAL(
        p.find(
            gumbopp::query
            (tag("html"))
            (tag("head"))
        ).size(),
        1
    );

    BOOST_CHECK_EQUAL(
        p.find(
            gumbopp::query
            (tag("html"))
            (tag("head"))
            (tag("meta"))
        ).size(),
        1
    );

    BOOST_CHECK_EQUAL(
        p.find(
            gumbopp::query
            (tag("html"))
            (tag("head"))
            (tag("meta"), attribute("charset"))
        ).size(),
        1
    );

    BOOST_CHECK_EQUAL(
        p.find(
            gumbopp::query
            (tag("html"))
            (tag("head"))
            (tag("meta"), attribute("charset", "UTF-8"))
        ).size(),
        1
    );

    BOOST_CHECK_EQUAL(
        p.find(
            gumbopp::query
            (tag("html"))
            (tag("head"))
            (tag("meta"), attribute("charset", "UTF-8###"))
        ).size(),
        0
    );
    BOOST_CHECK_EQUAL(
        p.find(
            gumbopp::query
            (tag("html###"))
            (tag("head"))
            (tag("meta"), attribute("charset", "UTF-8"))
        ).size(),
        0
    );
    BOOST_CHECK_EQUAL(
        p.find(
            gumbopp::query
            (tag("html"))
            (tag("head"))
            (tag("meta"), attribute("charset###", "UTF-8"))
        ).size(),
        0
    );

    BOOST_CHECK_EQUAL(
        p.flat_find(
            gumbopp::query
            ("h5")
        ).size(),
        1
    );
    BOOST_CHECK_EQUAL(
        p.flat_find(
            gumbopp::query
            ("div")
        ).size(),
        6
    );
    BOOST_CHECK_EQUAL(
        p.flat_find(
            gumbopp::query
            ("div", attribute("id", "1"))
        ).size(),
        2
    );
    print_t("done");
}

