#define BOOST_TEST_MODULE gumbopp

#include <boost/test/included/unit_test.hpp>

#include <gumbopp/parser.hpp>


BOOST_AUTO_TEST_CASE(access_members)
{
    gumbopp::parser p
    {
        R"(
            <html>
                <head>
                    <title>titletext</title>
                    <meta charset="UTF-8" />
                </head>
                <body class="classvalue">
                    <div id="0">
                        <div id="1">
                            <div id="2">
                                <h5>
                                    <a href="/">
                                        hreftext
                                    </a>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div id="0">
                        <div id="1"/>
                        <div id="2"/>
                    </div>
                </body>
            </html>
        )"
    };

    const auto print_node = [](const auto & n)
    {
        std::cout << "######################################\ntag = "
                  << n.tag()
                  << '\n' << n.start_tag()
                  << '\n' << n.end_tag()
                  << "\nattributes: " << std::endl;
        for (const auto& a : n.attributes())
        {
            std::cout << "    " << a.name() << " -> " << a.value() << std::endl;
        }
        std::cout << "text:" << std::endl;
        for (const auto& t : n.child_texts())
        {
            std::cout << '\'' << t.c_str() << '\'' << std::endl;
        }
        std::cout << "children:" << std::endl;
        for (const auto& e : n.child_elements())
        {
            std::cout << "    " << e.tag() << std::endl;
        }
    };

    using namespace gumbopp::query_node;
    const auto& html = p.root();
    {
        print_node(html);
        BOOST_CHECK_EQUAL(html.tag(), "html");
        BOOST_CHECK_EQUAL(html.attributes().size(), 0);
        BOOST_CHECK_EQUAL(html.child_texts().size(), 3);
        BOOST_REQUIRE_EQUAL(html.child_elements().size(), 2);

        const auto& head = html.child_elements().at(0);
        {
            print_node(head);
            BOOST_CHECK_EQUAL(head.tag(), "head");
            BOOST_CHECK_EQUAL(head.attributes().size(), 0);
            BOOST_CHECK_EQUAL(head.child_texts().size(), 3);
            BOOST_REQUIRE_EQUAL(head.child_elements().size(), 2);

            const auto& title = head.child_elements().at(0);
            {
                print_node(title);
                BOOST_CHECK_EQUAL(title.tag(), "title");
                BOOST_CHECK_EQUAL(title.attributes().size(), 0);
                BOOST_REQUIRE_EQUAL(title.child_texts().size(), 1);
                {
                    BOOST_CHECK_EQUAL(title.child_texts().at(0).c_str(), "titletext");
                }
                BOOST_REQUIRE_EQUAL(title.child_elements().size(), 0);
            }

            const auto& meta = head.child_elements().at(1);
            {
                print_node(meta);
                BOOST_CHECK_EQUAL(meta.tag(), "meta");
                BOOST_REQUIRE_EQUAL(meta.attributes().size(), 1);
                {
                    BOOST_CHECK(!meta.has_attribute("###"));
                    BOOST_REQUIRE(meta.has_attribute("charset"));
                    BOOST_CHECK_EQUAL(meta.attribute("charset").value(), "UTF-8");

                }
                BOOST_CHECK_EQUAL(meta.child_texts().size(), 0);
                BOOST_REQUIRE_EQUAL(meta.child_elements().size(), 0);
            }
        }
        const auto& body = html.child_elements().at(1);
        {
            print_node(body);
            BOOST_CHECK_EQUAL(body.tag(), "body");
            BOOST_REQUIRE_EQUAL(body.attributes().size(), 1);
            BOOST_CHECK_EQUAL(body.child_texts().size(), 3);
            BOOST_REQUIRE_EQUAL(body.child_elements().size(), 2);
        }
    }


}

